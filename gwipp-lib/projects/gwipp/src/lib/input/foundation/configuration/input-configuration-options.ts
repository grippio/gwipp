export interface InputConfigurationOptions {
  label?: string;
  fieldId?: string;
  infoLabel?: string;
  errorLabel?: string;
  optional?: boolean;
  inline?: boolean;
  maxLength?: number;
}
