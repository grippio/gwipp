export class IconNames {
  public static ANGLE_RIGHT = 'fas fa-angle-right';
  public static EXCLAMATION_TRIANGLE = 'fas fa-exclamation-triangle';
  public static EYE = 'fas fa-eye';
  public static EYE_SLASH = 'fas fa-eye-slash';
  public static SPINNER = 'fas fa-spinner';
  public static TIMES = 'fas fa-times';
  public static USER = 'fas fa-user';
  public static USER_CIRCLE = 'fas fa-user-circle';
}
